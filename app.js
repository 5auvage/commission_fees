const request = require('request-promise-native');
const fs = require('fs');
const moment = require('moment');
moment().format();

//load configurations
async function handleRequest(url) {
    try {
        const response = await request({
            uri: url,
            json: true
        });

        if (response.status == 200) {
            throw new Error('Bad response');
        }

        return await response
    } catch (err) {
        throw err;
    }
}

async function getParameters() {
    try {
        //get configurations
        const cashIn = await handleRequest('http://private-38e18c-uzduotis.apiary-mock.com/config/cash-in');
        const cashOutNatural = await handleRequest('http://private-38e18c-uzduotis.apiary-mock.com/config/cash-out/natural');
        const cashOutJuridical = await handleRequest('http://private-38e18c-uzduotis.apiary-mock.com/config/cash-out/juridical');
        const addRates = await handleRequest('http://private-38e18c-uzduotis.apiary-mock.com/rates');

        //call input as argument from command line
        const fileNames = process.argv.splice(2);
        const fileName = fileNames[0];
        const obj = fs.readFileSync(fileName, 'utf-8');
        const inputData = JSON.parse(obj);

        //variables
        const JPY = addRates.EUR['JPY'];
        const USD = addRates.EUR['USD'];
        const cashInPercents = cashIn.percents / 100;
        const cashInMaxAmount = cashIn.max['amount'];
        const cashOutPercentsNat = cashOutNatural.percents / 100;
        const cashOutWeekLimitNat = cashOutNatural.week_limit['amount']
        const cashOutPercentsJur = cashOutJuridical.percents / 100;
        const cashOutMinAmountJur = cashOutJuridical.min['amount'];

        //functions
        const round = (x) => {
            const num = parseFloat(Math.ceil(x * Math.pow(10, 2)) / Math.pow(10, 2)).toFixed(2);
            return num.toString();
        };

        function defineRate(currentRate, amount) {
            if (currentRate == "USD") {
                amount = amount / USD;
            }
            else if (currentRate == "JPY") {
                amount = amount / JPY
            }
            else {
                amount = amount
            }
            return round(amount)
        }

        function addValueInObject(userID, date, amount, currency) {
            var res = {};
            date = moment(date).isoWeek();

            let amountInEuros = defineRate(currency, amount);
            res = JSON.parse('{"user_id":"' + userID + '","week_number":"' + date + '","amount":"' + amountInEuros + '","currency":"' + currency + '"}');
            return res;
        }

        //calculate commission fees
        const dates = [];

        for (var key in inputData) {

            const currentRate = inputData[key].operation['currency'];

            if (inputData[key].type === 'cash_in') {
                const amount = inputData[key].operation['amount'];
                const commissions = defineRate(currentRate, amount) * cashInPercents
                if (commissions >= cashInMaxAmount) {
                     process.stdout.write(round(cashInMaxAmount) + '\n');
                }
                else {
                     process.stdout.write(round(commissions) + '\n');
                }
            }
            else {
                if (inputData[key].user_type === 'natural') {
                    const currentCashOut = inputData[key].operation['amount'];
                    const currentCashOutRated = defineRate(currentRate, currentCashOut);
                    const record = addValueInObject(inputData[key].user_id, inputData[key].date, inputData[key].operation['amount'], inputData[key].operation['currency']);

                    dates.push(record);

                    //Patikrinimas ar fizinis asmuo per savaitę išgrynino daugiau negu 1000 eurų
                    const getLastWeekNumberInArray = dates.sort(function (a, b) {
                        return a.week_number - b.week_number
                    })[dates.length - 1]

                    const get_all_Objects_With_Last_Week_Number = dates.filter(function (elem) {
                        return elem.week_number === getLastWeekNumberInArray.week_number;
                    })

                    const getLastUserID = get_all_Objects_With_Last_Week_Number.sort(function (a, b) {
                        return a.user_id - b.user_id
                    })[get_all_Objects_With_Last_Week_Number.length - 1]

                    const getAllWeekDaysOfLastUser = get_all_Objects_With_Last_Week_Number.filter(function (elem) {
                        return elem.user_id === getLastUserID.user_id;
                    })

                    const newArray = getAllWeekDaysOfLastUser;
                    //Gautas rezultatas

                    let userCashOutSumPerWeek = 0;
                    for (var key in newArray) {
                        userCashOutSumPerWeek += parseInt(newArray[key].amount);
                    }

                    if (userCashOutSumPerWeek - cashOutWeekLimitNat < 0) {
                        const commissions = 0;
                        process.stdout.write(round(commissions) + '\n');
                    }
                    else {
                        if (userCashOutSumPerWeek - cashOutWeekLimitNat > currentCashOut) {
                            const commissions = (currentCashOut) * cashOutPercentsNat;
                             process.stdout.write(round(commissions) + '\n');
                        }
                        else if (userCashOutSumPerWeek > cashOutWeekLimitNat && currentCashOut <= cashOutWeekLimitNat) {
                            const commissions = (userCashOutSumPerWeek - cashOutWeekLimitNat) * cashOutPercentsNat;
                             process.stdout.write(round(commissions) + '\n');
                        }
                        else {
                            const commissions = (currentCashOut - cashOutWeekLimitNat) * cashOutPercentsNat;
                             process.stdout.write(round(commissions) + '\n');
                        }
                    }
                }
                else {
                    const commissions = defineRate(currentRate, inputData[key].operation['amount']) * cashOutPercentsJur;
                    if (commissions < cashOutMinAmountJur) {
                         process.stdout.write(round(cashOutMinAmountJur) + '\n');
                    }
                    else {
                         process.stdout.write(round(commissions) + '\n');
                    }
                }
            }
        }

    } catch (err) {
        console.error(err);
    }
}

getParameters();